import argparse

from structlog import get_logger

from gnista_cli.influxdb_import import InfluxDbImport

from .gnista_websocket_connection import GnistaWebsocketConnection

log = get_logger()


def listen(args):
    gnista_url = args.url
    workspace_id = args.workspace_id
    log.info("Listening to gnista url", url=gnista_url)
    connection = GnistaWebsocketConnection(workspace_id=workspace_id, base_url=gnista_url)
    connection.connect_agent()


def simulate(args):
    gnista_url = args.url
    workspace_id = args.workspace_id
    influx_import = InfluxDbImport(workspace_id=workspace_id, gnista_url=gnista_url, influx_host="localhost")
    influx_import.start_simulation(database_name="test", measurement_name="test")


def main():
    parser = argparse.ArgumentParser(prog="PROG")
    subparsers = parser.add_subparsers(help="actions")

    parser_listen = subparsers.add_parser(
        "listen", help="listen for gnista commands", aliases=["l"]
    )
    parser_listen.add_argument("--workspace_id", metavar="{workspace_id}", nargs='?', type=str, help="workspaceId to listen to")
    parser_listen.add_argument("--url", metavar="{url}", nargs='?', type=str, help="url to listen to", default="https://app.gnista.io/")
    parser_listen.set_defaults(func=listen)

    parser_simulate = subparsers.add_parser(
        "simulate", help="simulate influx ingestion", aliases=["s"]
    )
    parser_simulate.add_argument("--url", metavar="{url}", nargs='?', type=str, help="url to listen to", default="https://app.gnista.io/")
    parser_simulate.set_defaults(func=simulate)

    log.info("Reading Commandline")

    try:
        args = parser.parse_args()
        if args.func is None:
            parser.print_help()
    # pylint:disable=W0703
    except Exception:
        parser.print_help()

    try:
        args.func(args)
    # pylint:disable=W0703
    except Exception as ex:
        log.critical("Excepton has been thrown", ex=ex, exc_info=True)
