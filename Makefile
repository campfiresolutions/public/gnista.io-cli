.DEFAULT_GOAL := help

# Show this help message
help:
	@cat $(MAKEFILE_LIST) | docker run --rm -i xanders/make-help

# prepare the build
prepare:
	pip install poetry
	poetry --version
	poetry config virtualenvs.in-project true
	poetry install -vv

# check styling
lint: prepare
	poetry run flake8 gnista_cli
	poetry run pylint gnista_cli --rcfile=pylint.rc

# check types
types: prepare
	poetry run mypy gnista_cli

format: prepare
	poetry run black .

# run all build targets
all: format lint types